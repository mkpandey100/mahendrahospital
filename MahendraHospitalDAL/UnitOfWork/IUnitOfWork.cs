﻿using MahendraHospitalDAL;
using MeroSahakariDAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeroSahakariDAL.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<PatientRegistration> PatientRegistrationRepository { get; }
        IRepository<Appointment> AppointmentRepository { get; }
    }
}
