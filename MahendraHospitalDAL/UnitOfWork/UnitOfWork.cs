﻿using MahendraHospitalDAL;
using MeroSahakariDAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeroSahakariDAL.UnitOfWork
{
    public class UnitOfWork: IUnitOfWork
    {
        MahendraHospitalEntities _context = null;
        public UnitOfWork()
        {
            _context = new MahendraHospitalEntities();
        }
        public UnitOfWork(MahendraHospitalEntities dbcontext)
        {
            _context = dbcontext;
        }
        public MahendraHospitalEntities Context { get { return _context; } }
        
        private IRepository<PatientRegistration> _Patient;
        public IRepository<PatientRegistration> PatientRegistrationRepository
        {
            get
            {
                return _Patient ?? (_Patient = new RepositoryBase<PatientRegistration>(_context));
            }
        }

        private IRepository<Appointment> _Appointment;
        public IRepository<Appointment> AppointmentRepository
        {
            get
            {
                return _Appointment ?? (_Appointment = new RepositoryBase<Appointment>(_context));
            }
        }
    }
}
