//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MahendraHospitalDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Refer
    {
        public decimal Id { get; set; }
        public Nullable<int> ReferById { get; set; }
        public string Name { get; set; }
        public string AccountCode { get; set; }
        public Nullable<decimal> CommissionPercentage { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> PartyId { get; set; }
        public Nullable<bool> Enable { get; set; }
    }
}
