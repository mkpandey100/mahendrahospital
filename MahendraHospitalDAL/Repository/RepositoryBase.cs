﻿using MahendraHospitalDAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeroSahakariDAL.Repository
{
    public class RepositoryBase<TEntity> : IRepository<TEntity> where TEntity : class
    {
        internal MahendraHospitalEntities context;
        private readonly DbSet<TEntity> _dbSet;
        private const bool ShareContext = false;

        #region Constructors
        public RepositoryBase()
        {
            context = new MahendraHospitalEntities();

        }
        public RepositoryBase(MahendraHospitalEntities context)
        {
            this.context = context;
            _dbSet = context.Set<TEntity>();
        }
        #endregion

        #region Protected properties
        protected DbSet<TEntity> DbSet
        {
            get
            {
                return context.Set<TEntity>();
            }
        }
        #endregion



        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Insert(TEntity entity)
        {
            _dbSet.Add(entity);
        }
               
        public TEntity Create(TEntity tEntity)
        {
            try
            {
                var newEntry = DbSet.Add(tEntity);
                context.SaveChanges();
                return newEntry;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public void Delete(object id)
        {
            TEntity entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
        }

        public void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == System.Data.Entity.EntityState.Detached)
            {
                _dbSet.Attach(entityToDelete);
            }
            _dbSet.Remove(entityToDelete);
        }

        public int Update(TEntity entityToUpdate)
        {
            if (!Exists(entityToUpdate))
            {
                _dbSet.Attach(entityToUpdate);
            }
            context.Entry(entityToUpdate).State = System.Data.Entity.EntityState.Modified;
            return context.SaveChanges();
        }

        public void Update(TEntity oldEntity, TEntity entityToUpdate)
        {
            if (!Exists(entityToUpdate))
            {
                _dbSet.Attach(entityToUpdate);
            }
            context.Entry(oldEntity).CurrentValues.SetValues(entityToUpdate);
            context.SaveChanges();
        }

        public virtual Boolean Exists(TEntity entity)
        {
            var objContext = ((IObjectContextAdapter)context).ObjectContext;
            var objSet = objContext.CreateObjectSet<TEntity>();
            var entityKey = objContext.CreateEntityKey(objSet.EntitySet.Name, entity);

            Object foundEntity;
            var exists = objContext.TryGetObjectByKey(entityKey, out foundEntity);
            if (exists)
            {
                objContext.Detach(foundEntity);
            }

            return (exists);
        }

    }
}