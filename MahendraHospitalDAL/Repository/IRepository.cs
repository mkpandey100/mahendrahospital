﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeroSahakariDAL.Repository
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        void Insert(TEntity entity);
        TEntity Create(TEntity tEntity);

        void Delete(object id);
        void Delete(TEntity entityToDelete);
                
        int Update(TEntity entityToUpdate);
        void Update(TEntity oldEntity, TEntity entityToUpdate);
    }
}
