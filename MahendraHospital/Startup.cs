﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MahendraHospital.Startup))]
namespace MahendraHospital
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
