﻿using MahendraHospitalDAL;
using MahendraHospitalServiceLayer.Implementation;
using MahendraHospitalServiceLayer.Interface;
using MahendraHospitalServiceLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MahendraHospital.Controllers.Appointment
{
    public class AppointmentController : Controller
    {
        MahendraHospitalEntities db = new MahendraHospitalEntities();
        IAppointmentService _cs;
        IPatientRegistrationService _iprs;

        // GET: Patient
        public ActionResult Index()
        {
            List<PatientRegistrationModel> Record = new List<PatientRegistrationModel>();
            _cs = new AppointmentService();
            _iprs = new PatientRegistrationService();
            Record = _iprs.GetAllPatient();
            return View(Record);
        }

        // GET: Patient/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AppointmentModel data = new AppointmentModel();
                    _cs = new AppointmentService();
                    data = _cs.Details(Convert.ToInt32(id));
                    return View(data);
                }
                else
                {
                    TempData["Danger"] = "Form Error";
                    return View();
                }

            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return View();
            }
        }

        // GET: Patient/Create
        public ActionResult Create()
        {
            ViewBag.country = new SelectList(db.Countries.ToList(), "Id", "Description");
            ViewBag.gender = new SelectList(db.Genders.ToList(), "Id", "Description");
            ViewBag.district = new SelectList(db.Districts.ToList(), "DistrictId", "DistrictNameEnglish");
            ViewBag.zone = new SelectList(db.Zones.ToList(), "ZoneId", "ZoneNameEnglish");
            ViewBag.relation = new SelectList(db.Relations.ToList(), "Id", "Description");
            ViewBag.refer = new SelectList(db.Refers.ToList(), "Id", "Name");
            ViewBag.registrationType = new SelectList(db.AppointmentPurposes.ToList(), "Id", "RegistrationType");
            ViewBag.profession = new SelectList(db.Professions.ToList(), "Id", "Description");
            ViewBag.organization = new SelectList(db.Organizations.ToList(), "Id", "Name");
            return View();
        }

        // POST: Patient/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PatientName,PhoneNo,Address,Age,Sex,RoomNo,DateAdmited,DateDischarged,IsEmployee,Status")] AppointmentModel appointment)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _cs = new AppointmentService();
                    _cs.AddAppointment(appointment);
                    TempData["Success"] = "Sucessfully Created";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form Error";
                    return View(appointment);
                }

            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return View(appointment);
            }
        }

        // GET: Patient/Edit/5s
        public ActionResult Edit(int? id)
        {
            _cs = new AppointmentService();
            AppointmentModel Record = new AppointmentModel();
            Record = _cs.GetOneAppointment(Convert.ToInt32(id));
            return View(Record);
        }

        // POST: Patient/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PatientName,PhoneNo,Address,Age,Sex,RoomNo,DateAdmited,DateDischarged,IsEmployee,Status")] AppointmentModel appointment)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _cs = new AppointmentService();
                    _cs.EditAppointment(appointment);
                    TempData["Success"] = "Sucessfully Edited";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form Error";
                    return View(appointment);
                }

            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return View(appointment);
            }
        }

        // GET: Patient/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _cs = new AppointmentService();
                    AppointmentModel Record = new AppointmentModel();
                    _cs.DeleteAppointment(Convert.ToInt32(id));
                    TempData["Success"] = "Deleted Sucessfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form Error";
                    return View();
                }

            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return View();

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}