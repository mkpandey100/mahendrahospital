﻿using MahendraHospitalDAL;
using MahendraHospitalServiceLayer.Implementation;
using MahendraHospitalServiceLayer.Interface;
using MahendraHospitalServiceLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MahendraHospital.Controllers.Patient
{
    public class PatientRegistrationController : Controller
    {
        MahendraHospitalEntities db = new MahendraHospitalEntities();
        IPatientRegistrationService _cs;

        // GET: Patient
        public ActionResult Index()
        {
            List<PatientRegistrationModel> Record = new List<PatientRegistrationModel>();
            _cs = new PatientRegistrationService();
            Record = _cs.GetAllPatient();
            return View(Record);
        }

        // GET: Patient/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PatientRegistrationModel data = new PatientRegistrationModel();
                    _cs = new PatientRegistrationService();
                    data = _cs.Details(Convert.ToInt32(id));
                    return View(data);
                }
                else
                {
                    TempData["Danger"] = "Form Error";
                    return View();
                }

            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return View();
            }
        }

        // GET: Patient/Create
        public ActionResult Create()
        {
            ViewBag.country = new SelectList(db.Countries.ToList(), "Id", "Description");
            ViewBag.gender = new SelectList(db.Genders.ToList(), "Id", "Description");
            ViewBag.district = new SelectList(db.Districts.ToList(), "DistrictId", "DistrictNameEnglish");
            ViewBag.zone = new SelectList(db.Zones.ToList(), "ZoneId", "ZoneNameEnglish");
            ViewBag.relation = new SelectList(db.Relations.ToList(), "Id", "Description");
            ViewBag.refer = new SelectList(db.Refers.ToList(), "Id", "Name");
            ViewBag.registrationType = new SelectList(db.PatientRegistrationTypes.ToList(), "Id", "RegistrationType");
            ViewBag.profession = new SelectList(db.Professions.ToList(), "Id", "Description");
            ViewBag.organization = new SelectList(db.Organizations.ToList(), "Id", "Name");
            return View();
        }

        // POST: Patient/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PatientName,PhoneNo,Address,Age,Sex,RoomNo,DateAdmited,DateDischarged,IsEmployee,Status")] PatientRegistrationModel patientModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _cs = new PatientRegistrationService();
                    _cs.AddPatient(patientModel);
                    TempData["Success"] = "Sucessfully Created";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form Error";
                    return View(patientModel);
                }

            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return View(patientModel);
            }
        }

        // GET: Patient/Edit/5s
        public ActionResult Edit(int? id)
        {
            _cs = new PatientRegistrationService();
            PatientRegistrationModel Record = new PatientRegistrationModel();
            Record = _cs.GetOnePatient(Convert.ToInt32(id));
            return View(Record);
        }

        // POST: Patient/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PatientName,PhoneNo,Address,Age,Sex,RoomNo,DateAdmited,DateDischarged,IsEmployee,Status")] PatientRegistrationModel patientModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _cs = new PatientRegistrationService();
                    _cs.EditPatient(patientModel);
                    TempData["Success"] = "Sucessfully Edited";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form Error";
                    return View(patientModel);
                }

            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return View(patientModel);
            }
        }

        // GET: Patient/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _cs = new PatientRegistrationService();
                    PatientRegistrationModel Record = new PatientRegistrationModel();
                    _cs.DeletePatient(Convert.ToInt32(id));
                    TempData["Success"] = "Deleted Sucessfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form Error";
                    return View();
                }

            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return View();

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}