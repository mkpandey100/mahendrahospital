﻿using MahendraHospitalServiceLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MahendraHospitalServiceLayer.Interface
{
    public interface IAppointmentService
    {
        void AddAppointment(AppointmentModel appointment);
        void Edit(int id);
        void EditAppointment(AppointmentModel appointment);
        void DeleteAppointment(int id);
        List<AppointmentModel> GetAllAppointment();
        AppointmentModel GetOneAppointment(int id);
        AppointmentModel Details(int id);
        List<AppointmentModel> FindByName(string appointment);
    }
}
