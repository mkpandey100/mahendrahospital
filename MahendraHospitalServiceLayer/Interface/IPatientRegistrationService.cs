﻿using MahendraHospitalServiceLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MahendraHospitalServiceLayer.Interface
{
    public interface IPatientRegistrationService
    {
        void AddPatient(PatientRegistrationModel patient);
        void Edit(int id);
        void EditPatient(PatientRegistrationModel patient);
        void DeletePatient(int id);
        List<PatientRegistrationModel> GetAllPatient();
        PatientRegistrationModel GetOnePatient(int id);
        PatientRegistrationModel Details(int id);
        List<PatientRegistrationModel> FindByName(string patientName);
    }
}
