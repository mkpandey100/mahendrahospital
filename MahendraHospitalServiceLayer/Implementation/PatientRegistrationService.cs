﻿using MahendraHospitalDAL;
using MahendraHospitalServiceLayer.Interface;
using MahendraHospitalServiceLayer.Model;
using MeroSahakariDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MahendraHospitalServiceLayer.Implementation
{
    public class PatientRegistrationService : IPatientRegistrationService
    {

        MahendraHospitalEntities Context = new MahendraHospitalEntities();

        #region Constructor
        private readonly IUnitOfWork _unitOfWork;
        public PatientRegistrationService(IUnitOfWork unitOfWork = null)
        {
            _unitOfWork = unitOfWork ?? new UnitOfWork();
        }
        #endregion

        public void AddPatient(PatientRegistrationModel patient)
        {
            PatientRegistration Record = Mapper.PatientRegistrationMapper.ModelToDb(patient);
            _unitOfWork.PatientRegistrationRepository.Create(Record);
        }

        public void DeletePatient(int id)
        {
            _unitOfWork.PatientRegistrationRepository.Delete(id);
        }

        public PatientRegistrationModel Details(int id)
        {
            PatientRegistration Record = Context.PatientRegistrations.Where(x => x.Id == id).FirstOrDefault();
            PatientRegistrationModel RecordData = Mapper.PatientRegistrationMapper.DbToModel(Record);
            return RecordData;
        }

        public void Edit(int id)
        {
            PatientRegistration patient = new PatientRegistration() { Id = id };
            Context.PatientRegistrations.Find(id);
            Context.SaveChanges();
        }

        public void EditPatient(PatientRegistrationModel patient)
        {
            PatientRegistration Record = Mapper.PatientRegistrationMapper.ModelToDb(patient);
            _unitOfWork.PatientRegistrationRepository.Update(Record);
        }

        public List<PatientRegistrationModel> FindByName(string patientName)
        {
            List<PatientRegistration> Record = (from x in Context.PatientRegistrations where x.FirstName.Contains(patientName) select x).ToList();
            List<PatientRegistrationModel> ReturnRecord = Mapper.PatientRegistrationMapper.DbListToModelList(Record);
            return ReturnRecord;
        }

        public List<PatientRegistrationModel> GetAllPatient()
        {
            List<PatientRegistration> Record = Context.PatientRegistrations.ToList();
            List<PatientRegistrationModel> ReturnRecord = Mapper.PatientRegistrationMapper.DbListToModelList(Record);
            return ReturnRecord;
        }

        public PatientRegistrationModel GetOnePatient(int id)
        {
            PatientRegistration Record = Context.PatientRegistrations.Where(x => x.Id == id).FirstOrDefault();
            PatientRegistrationModel ReturnRecord = Mapper.PatientRegistrationMapper.DbToModel(Record);
            return ReturnRecord;
        }
    }
}
