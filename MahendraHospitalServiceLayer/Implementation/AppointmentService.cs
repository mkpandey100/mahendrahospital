﻿using MahendraHospitalServiceLayer.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MahendraHospitalServiceLayer.Model;
using MahendraHospitalDAL;
using MeroSahakariDAL.UnitOfWork;

namespace MahendraHospitalServiceLayer.Implementation
{
    public class AppointmentService : IAppointmentService
    {
        MahendraHospitalEntities Context = new MahendraHospitalEntities();

        #region Constructor
        private readonly IUnitOfWork _unitOfWork;
        public AppointmentService(IUnitOfWork unitOfWork = null)
        {
            _unitOfWork = unitOfWork ?? new UnitOfWork();
        }
        #endregion
        

        public void AddAppointment(AppointmentModel appointment)
        {
            Appointment Record = Mapper.AppointmentMapper.ModelToDb(appointment);
            _unitOfWork.AppointmentRepository.Create(Record);
        }

        public void DeleteAppointment(int id)
        {
            _unitOfWork.AppointmentRepository.Delete(id);
        }

        public AppointmentModel Details(int id)
        {
            Appointment Record = Context.Appointments.Where(x => x.Id == id).FirstOrDefault();
            AppointmentModel RecordData = Mapper.AppointmentMapper.DbToModel(Record);
            return RecordData;
        }

        public void Edit(int id)
        {
            Appointment patient = new Appointment() { Id = id };
            Context.Appointments.Find(id);
            Context.SaveChanges();
        }

        public void EditAppointment(AppointmentModel appointment)
        {
            Appointment Record = Mapper.AppointmentMapper.ModelToDb(appointment);
            _unitOfWork.AppointmentRepository.Update(Record);
        }

        public List<AppointmentModel> FindByName(string appointment)
        {
            List<Appointment> Record = (from x in Context.Appointments where x.FileNumber.Contains(appointment) select x).ToList();
            List<AppointmentModel> ReturnRecord = Mapper.AppointmentMapper.DbListToModelList(Record);
            return ReturnRecord;
        }

        public List<AppointmentModel> GetAllAppointment()
        {
            List<Appointment> Record = Context.Appointments.ToList();
            List<AppointmentModel> ReturnRecord = Mapper.AppointmentMapper.DbListToModelList(Record);
            return ReturnRecord;
        }

        public AppointmentModel GetOneAppointment(int id)
        {
            Appointment Record = Context.Appointments.Where(x => x.Id == id).FirstOrDefault();
            AppointmentModel ReturnRecord = Mapper.AppointmentMapper.DbToModel(Record);
            return ReturnRecord;
        }
    }
}
