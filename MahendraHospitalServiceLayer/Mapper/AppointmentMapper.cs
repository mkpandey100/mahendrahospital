﻿using MahendraHospitalDAL;
using MahendraHospitalServiceLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MahendraHospitalServiceLayer.Mapper
{
    public class AppointmentMapper
    {
        public static List<AppointmentModel> DbListToModelList(List<Appointment> Record)
        {
            List<AppointmentModel> ModelList = new List<AppointmentModel>();
            foreach (var Row in Record)
            {
                AppointmentModel ModelRecord = new AppointmentModel
                {
                    Id = Row.Id,
                    FileNumber = Row.FileNumber,
                    AppointmentBy = Row.AppointmentBy,
                    AppointmentByRelationId = Row.AppointmentByRelationId,
                    AppointmentTypeId = Row.AppointmentTypeId,
                    AppointMethodId =Row.AppointMethodId,
                    Consultant = Row.Consultant,
                    DepartmentId = Row.DepartmentId,
                    EndDate = Row.EndDate,
                    HealthPackageId = Row.HealthPackageId,
                    PatientId = Row.PatientId,
                    PurposeId = Row.PurposeId,
                    Remarks  = Row.Remarks,
                    RemindBefore = Row.RemindBefore,
                    ReminderStatus = Row.ReminderStatus,
                    RemindOn = Row.RemindOn,
                    ResponsiblePerson = Row.ResponsiblePerson,
                    SchemeId = Row.SchemeId,
                    SpecialityId = Row.SpecialityId,
                    StartDate = Row.StartDate,
                    TreatmentId = Row.TreatmentId,
                    UnitId = Row.UnitId,
                    Urgency = Row.Urgency,
                    Status = Row.Status
                };
                ModelList.Add(ModelRecord);
            }
            return ModelList;
        }

        public static Appointment ModelToDb(AppointmentModel Record)
        {
            Appointment ReturnRecord = new Appointment
            {
                Id = Record.Id,
                FileNumber = Record.FileNumber,
                AppointmentBy = Record.AppointmentBy,
                AppointmentByRelationId = Record.AppointmentByRelationId,
                AppointmentTypeId = Record.AppointmentTypeId,
                AppointMethodId = Record.AppointMethodId,
                Consultant = Record.Consultant,
                DepartmentId = Record.DepartmentId,
                EndDate = Record.EndDate,
                HealthPackageId = Record.HealthPackageId,
                PatientId = Record.PatientId,
                PurposeId = Record.PurposeId,
                Remarks = Record.Remarks,
                RemindBefore = Record.RemindBefore,
                ReminderStatus = Record.ReminderStatus,
                RemindOn = Record.RemindOn,
                ResponsiblePerson = Record.ResponsiblePerson,
                SchemeId = Record.SchemeId,
                SpecialityId = Record.SpecialityId,
                StartDate = Record.StartDate,
                TreatmentId = Record.TreatmentId,
                UnitId = Record.UnitId,
                Urgency = Record.Urgency,
                Status = Record.Status
            };
            return ReturnRecord;
        }

        public static AppointmentModel DbToModel(Appointment Record)
        {
            AppointmentModel ReturnRecord = new AppointmentModel
            {
                Id = Record.Id,
                FileNumber = Record.FileNumber,
                AppointmentBy = Record.AppointmentBy,
                AppointmentByRelationId = Record.AppointmentByRelationId,
                AppointmentTypeId = Record.AppointmentTypeId,
                AppointMethodId = Record.AppointMethodId,
                Consultant = Record.Consultant,
                DepartmentId = Record.DepartmentId,
                EndDate = Record.EndDate,
                HealthPackageId = Record.HealthPackageId,
                PatientId = Record.PatientId,
                PurposeId = Record.PurposeId,
                Remarks = Record.Remarks,
                RemindBefore = Record.RemindBefore,
                ReminderStatus = Record.ReminderStatus,
                RemindOn = Record.RemindOn,
                ResponsiblePerson = Record.ResponsiblePerson,
                SchemeId = Record.SchemeId,
                SpecialityId = Record.SpecialityId,
                StartDate = Record.StartDate,
                TreatmentId = Record.TreatmentId,
                UnitId = Record.UnitId,
                Urgency = Record.Urgency,
                Status = Record.Status
            };
            return ReturnRecord;
        }
    }
}
