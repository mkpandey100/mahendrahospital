﻿using MahendraHospitalDAL;
using MahendraHospitalServiceLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MahendraHospitalServiceLayer.Mapper
{
    public class PatientRegistrationMapper
    {
        public static List<PatientRegistrationModel> DbListToModelList(List<PatientRegistration> Record)
        {
            List<PatientRegistrationModel> ModelList = new List<PatientRegistrationModel>();
            foreach (var Row in Record)
            {
                PatientRegistrationModel ModelRecord = new PatientRegistrationModel
                {
                    Id = Row.Id,
                    FileNumber = Row.FileNumber,
                    FirstName = Row.FirstName,
                    LastName = Row.LastName,
                    Age = Row.Age,
                    CodeNumber = Row.CodeNumber,
                    CountryId = Row.CountryId,
                    Mobile = Row.Mobile,
                    PermanentAddress = Row.PermanentAddress,
                    PermanentDistrictId = Row.PermanentDistrictId,
                    PermanentZoneId = Row.PermanentZoneId,
                    TemporaryAddress = Row.TemporaryAddress,
                    TemporaryDistrictId = Row.TemporaryDistrictId,
                    TemporaryZoneId = Row.TemporaryZoneId,
                    GenderId = Row.GenderId,
                    Email = Row.Email,
                    Phone = Row.Phone,
                    MaritalStatusId = Row.MaritalStatusId,
                    RegistrationTypeId = Row.RegistrationTypeId,
                    ProfessionId = Row.ProfessionId,
                    DateOfBirth = Row.DateOfBirth,
                    DateOfBirthBS = Row.DateOfBirthBS,
                    ReferById = Row.ReferById,
                    OrganizationId = Row.OrganizationId,
                    RegistrationDate = Row.RegistrationDate,
                    RegistrationDateBS = Row.RegistrationDateBS,
                    CreatedBy = Row.CreatedBy,
                    CreatedDate = Row.CreatedDate,
                    UpdatedBy = Row.UpdatedBy,
                    UpdatedDate = Row.UpdatedDate,
                    Status  = Row.Status
                };
                ModelList.Add(ModelRecord);
            }
            return ModelList;
        }

        public static PatientRegistration ModelToDb(PatientRegistrationModel Record)
        {
            PatientRegistration ReturnRecord = new PatientRegistration
            {
                Id = Record.Id,
                FileNumber = Record.FileNumber,
                FirstName = Record.FirstName,
                LastName = Record.LastName,
                Age = Record.Age,
                CodeNumber = Record.CodeNumber,
                CountryId = Record.CountryId,
                Mobile = Record.Mobile,
                PermanentAddress = Record.PermanentAddress,
                PermanentDistrictId = Record.PermanentDistrictId,
                PermanentZoneId = Record.PermanentZoneId,
                TemporaryAddress = Record.TemporaryAddress,
                TemporaryDistrictId = Record.TemporaryDistrictId,
                TemporaryZoneId = Record.TemporaryZoneId,
                GenderId = Record.GenderId,
                Email = Record.Email,
                Phone = Record.Phone,
                MaritalStatusId = Record.MaritalStatusId,
                RegistrationTypeId = Record.RegistrationTypeId,
                ProfessionId = Record.ProfessionId,
                DateOfBirth = Record.DateOfBirth,
                DateOfBirthBS = Record.DateOfBirthBS,
                ReferById = Record.ReferById,
                OrganizationId = Record.OrganizationId,
                RegistrationDate = Record.RegistrationDate,
                RegistrationDateBS = Record.RegistrationDateBS,
                CreatedBy = Record.CreatedBy,
                CreatedDate = Record.CreatedDate,
                UpdatedBy = Record.UpdatedBy,
                UpdatedDate = Record.UpdatedDate,
                Status = Record.Status
            };
            return ReturnRecord;
        }

        public static PatientRegistrationModel DbToModel(PatientRegistration Record)
        {
            PatientRegistrationModel ReturnRecord = new PatientRegistrationModel
            {
                Id = Record.Id,
                FileNumber = Record.FileNumber,
                FirstName = Record.FirstName,
                LastName = Record.LastName,
                Age = Record.Age,
                CodeNumber = Record.CodeNumber,
                CountryId = Record.CountryId,
                Mobile = Record.Mobile,
                PermanentAddress = Record.PermanentAddress,
                PermanentDistrictId = Record.PermanentDistrictId,
                PermanentZoneId = Record.PermanentZoneId,
                TemporaryAddress = Record.TemporaryAddress,
                TemporaryDistrictId = Record.TemporaryDistrictId,
                TemporaryZoneId = Record.TemporaryZoneId,
                GenderId = Record.GenderId,
                Email = Record.Email,
                Phone = Record.Phone,
                MaritalStatusId = Record.MaritalStatusId,
                RegistrationTypeId = Record.RegistrationTypeId,
                ProfessionId = Record.ProfessionId,
                DateOfBirth = Record.DateOfBirth,
                DateOfBirthBS = Record.DateOfBirthBS,
                ReferById = Record.ReferById,
                OrganizationId = Record.OrganizationId,
                RegistrationDate = Record.RegistrationDate,
                RegistrationDateBS = Record.RegistrationDateBS,
                CreatedBy = Record.CreatedBy,
                CreatedDate = Record.CreatedDate,
                UpdatedBy = Record.UpdatedBy,
                UpdatedDate = Record.UpdatedDate,
                Status = Record.Status
            };
            return ReturnRecord;
        }
    }
}
