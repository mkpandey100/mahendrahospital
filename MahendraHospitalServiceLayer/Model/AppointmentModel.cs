﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MahendraHospitalServiceLayer.Model
{
    public class AppointmentModel
    {
        public decimal Id { get; set; }
        public decimal? PatientId { get; set; }
        public string FileNumber { get; set; }
        public int? AppointmentTypeId { get; set; }
        public string AppointmentBy { get; set; }
        public int? AppointmentByRelationId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? SpecialityId { get; set; }
        public int? TreatmentId { get; set; }
        public int? DepartmentId { get; set; }
        public int? UnitId { get; set; }
        public int? HealthPackageId { get; set; }
        public int? SchemeId { get; set; }
        public int? PurposeId { get; set; }
        public string Remarks { get; set; }
        public DateTime? RemindOn { get; set; }
        public int? Urgency { get; set; }
        public int? AppointMethodId { get; set; }
        public int? Status { get; set; }
        public int? ReminderStatus { get; set; }
        public int? RemindBefore { get; set; }
        public int? ResponsiblePerson { get; set; }
        public int? Consultant { get; set; }
    }
}
