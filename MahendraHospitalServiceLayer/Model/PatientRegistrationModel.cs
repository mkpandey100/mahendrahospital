﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MahendraHospitalServiceLayer.Model
{
    public class PatientRegistrationModel
    {
        public int Id { get; set; }
        public string CodeNumber { get; set; }
        public string FileNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string DateOfBirthBS { get; set; }
        public int? Age { get; set; }
        public int? GenderId { get; set; }
        public int? MaritalStatusId { get; set; }
        public int? RegistrationTypeId { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public DateTime? RegistrationDateBS { get; set; }
        public string TemporaryAddress { get; set; }
        public int? TemporaryDistrictId { get; set; }
        public int? TemporaryZoneId { get; set; }
        public string PermanentAddress { get; set; }
        public int? PermanentDistrictId { get; set; }
        public int? PermanentZoneId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public int? CountryId { get; set; }
        public int? ProfessionId { get; set; }
        public int? OrganizationId { get; set; }
        public int? ReferById { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? Status { get; set; }
    }
}
